package br.com.itau;

public class Gerente extends Funcionario {
    public int quantidadeDeFuncionarios;

    public Gerente(String nome, int idade, int cpf, int racf, double salario, int quantidadeDeFuncionarios) {
        super(nome, idade, cpf, racf, salario);
        this.quantidadeDeFuncionarios = quantidadeDeFuncionarios;
    }

    @Override
    public double getBonificacao() {
        return super.getSalario() * 0.15;
    }
}
