package br.com.itau;

public class Pessoa {
    public String nome;
    public int idade;
    public int cpf;

    public Pessoa(String nome, int idade, int cpf) {
        this.nome = nome;
        this.idade = idade;
        this.cpf = cpf;
    }
}
