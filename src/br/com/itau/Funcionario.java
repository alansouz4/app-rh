package br.com.itau;

public class Funcionario extends Pessoa{
    public int racf;
    private double salario;

    public Funcionario(String nome, int idade, int cpf, int racf, double salario) {
        super(nome, idade, cpf);
        this.racf = racf;
        this.salario = salario;
    }

    public double getSalario() {
        return salario;
    }

    public double getBonificacao(){
        return salario * 0.5;
    }
}
